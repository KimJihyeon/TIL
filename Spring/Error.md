

# Ajax request return 200, but error

 Ajax request 리턴 status가 200인데, 에러가 나는 문제 발생.

     $.ajax({
	       url: "/userLogin", 
	       method: "POST",
	       data: formData,
	       dataType: 'json',
	       processData: false, 
	       contentType: false,
	       success: function (response) {
                  
		     window.location.href = "/";
	        }, error: function (jqXHR) {
	        	console.log("에러? "+JSON.stringify(jqXHR));
	        }
	    })
    })
        

<리턴 메세지>

    {"readyState":4,"responseText":"","status":200,"statusText":"parsererror"}

 - 해결방법
	 - ajax리턴을 'json'방식으로 받겠다고 했는데, 다른형식의 리턴값이 와서 발생하는 문제. (json 리턴 부분을 잘못 주석처리ㅠㅠ)
	 - json 형식으로 리턴해주면 해결  완료!

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwMDUwOTUzNDJdfQ==
-->