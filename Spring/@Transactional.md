


**@Transactional 어노테이션을 이용한 트랜잭션 적용**

 - Mybatis 설정 클래스 파일에 추가
 
 <Transaction을 사용하기 위한 설정>
   
   

	     @Bean
	        public DataSourceTransactionManager transactionManager(){
	    	    return new DataSourceTransactionManager(DataSource());
	    	}


 < 서비스 레이어에서 @Transactional 어노테이션 적용>
 
		   @Service
		**@Transactional**
		public class UserServiceImpl implements UserService{
		...
		}



 - 메소드 뿐 아니라 인터페이스, 클래스에도 선언 가능. 
	 - 메소드에 선언된 @Transactional 설정이 최우선순위.
	 - 특정 설정을 메소드에 적용할 수 있다. 

**주의: MySQL의 경우 DB엔진을 InnoDB로 설정해줘야 함. MyISAM은 트랜잭션 적용안됨.




# Transaction 속성

@Transactional 어노테이션을 적용했으나 트랜잭션 동작이 안되는 상황이 발생해서 찾아보니 특정 예외가 발생했을 때에는 Rollback 설정을 해줘야한다고 함.

	@Transactional(rollbackFor=Exception.class)
	@Override
	public int deleteInformation(User user) throws Exception {
		.....
		if(deleteResult != 1) {
    		throw new Exception("PATIENT_REMOVE_FAIL");
    	}
	}

위 코드에 보면 내가 만든 예외가 발생했을때 트랜잭션이 적용이 안됐음.  
해당 메소드에 @Transactional(rollbackFor=Exception.class) 어노테이션과 설정을 적용하니 트랜잭션 구현 완료!

--------------------
위에 상황을 겪고 Transaction 속성을 정리할 필요가 있다고 생각함.

**isolation**

 - Transacion들이 다른 Transaction의 방해로부터 보호되는 정도

**propagation**

 - Transaction 전파규칙 정의

**readOnly**

 - Transaction을 읽기 전용 모드로 처리

**rollbackFor**

 - 정의된 Exception에 대해서 rollback 수행

**noRollbackFor**

 - 정의된 Exception에 대해서 rollback 수행하지 않음

**timeout**

 - 지정한 시간 내에 해당 메소드 수행이 완료되지 않을 경우 rollback 수행

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY0NTg2MDYyOCwxOTEwNzEwMDYyLC0xNj
g3ODU3MTg5LC0xODExMzY4ODE3XX0=
-->