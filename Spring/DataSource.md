# DataSource

 - DataSource란?
	 - JDBC의 설정 내용(DB와 Connection 정보)을 가지고 있는 것으로 DB와의 연동 작업에 꼭 필요.
	 - 스프링은 DataSouce를 통해 DB와 연결을 획득한다.
	 - DB Connection Pooling 기능을 갖는다.
	 - 트랜잭션을 처리한다.




> Connection Pool : Connection 객체를 생성해놓고 DB 작업 시 사용 후 다시 반납하는 형식. 데이터베이스에 연결 작업은 시간이 걸리기 때문에 효율적인 방법이다.

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk0MjU2Njg2MywtMjk3NTIwMjQxLDIwND
AyOTc2MjJdfQ==
-->